const gulp = require('gulp');
const browserSync = require('browser-sync');
const reload = browserSync.reload;
const sass = require('gulp-sass');
const concat = require('gulp-concat');

const src = {
    sass: './src/sass/**/*.scss',
    jquery: './vendors/jquery/dist/jquery.min.js',
    tether: {
        css: './vendors/tether/dist/css/tether.min.css',
        js: './vendors/tether/dist/js/tether.min.js'
    },
    bootstrap: {
        css: './vendors/bootstrap/dist/css/bootstrap.min.css',
        js: './vendors/bootstrap/dist/js/bootstrap.min.js'
    },
    fa: {
        css: './vendors/font-awesome/css/font-awesome.min.css',
        fonts: './vendors/font-awesome/fonts/*'
    },
    owl: {
        css: './vendors/owl.carousel/dist/assets/owl.carousel.min.css',
        imgs: [
            './vendors/owl.carousel/dist/assets/ajax-loader.gif',
            './vendors/owl.carousel/dist/assets/owl.video.play.png'
        ],
        js: './vendors/owl.carousel/dist/owl.carousel.min.js'
    }
};

const dest = {
    css: './src/dist/css',
    js: './src/dist/js',
    fonts: './src/dist/fonts'
};

gulp.task('serve', () => {
    browserSync({
        notify: false,
        server: {
            baseDir: './src'
        }
    });

    gulp.watch(['./src/**/*.html'], reload);
    gulp.watch(['./src/**/*.js'], reload);
    gulp.watch(['./src/**/*.css'], reload);
    gulp.watch(['./src/**/*.scss'], ['sass'], reload);
});

gulp.task('sass', () => {
    return gulp.src(src.sass)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(dest.css));
});

gulp.task('concat-css', () => {
    const srcs = [
        src.tether.css,
        src.bootstrap.css,
        src.fa.css,
        src.owl.css
    ];
    return gulp.src(srcs)
        .pipe(concat('vendors.css'))
        .pipe(gulp.dest(dest.css));
});

gulp.task('concat-js', () => {
    const srcs = [
        src.jquery,
        src.tether.js,
        src.bootstrap.js,
        src.owl.js
    ];
    return gulp.src(srcs)
        .pipe(concat('vendors.js'))
        .pipe(gulp.dest(dest.js));
});

gulp.task('move-fonts', () => {
    return gulp.src([src.fa.fonts])
        .pipe(gulp.dest(dest.fonts));
});

gulp.task('owl', () => {
    return gulp.src(src.owl.imgs)
        .pipe(gulp.dest(dest.css));
});

gulp.task('vendors', ['concat-css', 'concat-js', 'move-fonts', 'owl']);

gulp.task('default', ['vendors', 'sass'], () => {
    gulp.start('serve');
});