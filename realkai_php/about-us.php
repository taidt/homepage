<div class="page-content">
            <header class="sub-page">
                <div class="mask">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h1>Maximise Your Websites Profits</h1>
                                <h2>Enjoy the real benefits of automatic advertising with us. We are happy to help you achieve your
                                    desired results.</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <section class="block bg-blue">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h1 class="text text-center mb-5">How AdRoll Retargeting Works</h1>
                        </div>
                    </div>
                    <div class="row list-block">
                        <div class="col-md-4">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-step1.png" alt="Card image cap">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text"><i>Some quick example text to build on the card title and make up the bulk of the card's content.</i></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-step2.png" alt="Card image cap">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text"><i>Some quick example text to build on the card title and make up the bulk of the card's content.</i></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-step3.png" alt="Card image cap">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text"><i>Some quick example text to build on the card title and make up the bulk of the card's content.</i></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="block bg-white">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text mb-4">
                                <h1>Leading-edge technology</h1>
                                <p>A powerful GUI platform to make your API development faster & easier, from building API requests through testing, documentation and sharing.</p>
                                <p>We recommend the free Postman App for Mac, Windows, Linux or Chrome</p>
                                <a href="#" class="btn btn-primary">Content</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <figure>
                                <img src="images/express-api-development.png" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
            </section>
            <section class="block bg-light-gray">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h1 class="text text-center mb-5">How AdRoll Retargeting Works</h1>
                        </div>
                    </div>
                    <div class="row list-block">
                        <div class="col-md-4">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-step1.png" alt="Card image cap">
                                <div class="card-block">
                                    <p class="card-text"><i>Some quick example text to build on the card title and make up the bulk of the card's content.</i></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-step2.png" alt="Card image cap">
                                <div class="card-block">
                                    <p class="card-text"><i>Some quick example text to build on the card title and make up the bulk of the card's content.</i></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-step3.png" alt="Card image cap">
                                <div class="card-block">
                                    <p class="card-text"><i>Some quick example text to build on the card title and make up the bulk of the card's content.</i></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="block" id="get-started">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="text">
                                <h1>Get Started</h1>
                                <p>Postman doesn’t require learning a new language, complicated UI, or new workflows. Developers
                                    can start using Postman immediately to make API development faster & easier.</p>
                                <a href="#" class="btn btn-primary">Become Our Publisher</a>
                            </div>
                        </div>
                        <div class="col-md-7"></div>
                    </div>
                </div>
            </section>
        </div>