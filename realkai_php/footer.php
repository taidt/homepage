		<!--BLOCK FOOTER-->
        <footer class="block bg-gray">
            <div class="container">
                <div class="footer-item mb-5 links">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="menu-item">
                                <ul class="company-info">
                                    <li class="title mb-1">Singapore office</li>
                                    <li class="link-item">
                                        <span>60 Paya Lebar Road,</span>
                                    </li>
                                    <li class="link-item">
                                        <span>#08-43 Paya Lebar Square,</span>
                                    </li>
                                    <li class="link-item">
                                        <span>Singapore 409051</span>
                                    </li>
                                    <li class="link-item">
                                        <span>E-mail: <strong>supports@realkai.com</strong></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="menu-item">
                                <ul>
                                    <li class="title mb-1">Publishers</li>
                                    <li class="link-item">
                                        <a href="/ad-formats.html">Ad Formats</a>
                                    </li>
                                    <li class="link-item">
                                        <a href="/realtime-bidding.html">Realtime Bidding</a>
                                    </li>
                                    <li class="link-item">
                                        <a href="/term.html">Payout Term</a>
                                    </li>
                                    <li class="link-item">
                                        <a href="/term.html">FAQs</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="menu-item">
                                <ul>
                                    <li class="title mb-1">Company</li>
                                    <li class="link-item">
                                        <a href="/about-us.html">About Us</a>
                                    </li>
                                    <li class="link-item">
                                        <a href="/about-us.html">What we do</a>
                                    </li>
                                    <li class="link-item">
                                        <a href="/join-us.html">Work at Realkai</a>
                                    </li>
                                    <li class="link-item">
                                        <a href="/">Realkai on Facebook</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="footer-item mt-5 copyright">
                    <div class="row">
                        <div class="col-sm-8">
                            <span class="mr-4">© 2017 Realkai .PTE .LTD.</span>
                            <span>All Rights Reserved</span>
                        </div>
                        <div class="col-sm-4">
                            <ul class="social-links text-right">
                                <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                <li><a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script type="text/javascript" src="dist/js/vendors.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</body>

</html>