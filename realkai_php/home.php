<div class="page-content">
            <header>
                <div class="mask">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <h1>Maximise Your Websites Profits</h1>
                                <h2>Enjoy the real benefits of automatic advertising with us. We are happy to help you achieve your
                                    desired results.</h2>
                                <div class="links">
                                    <a href="" class="btn btn-primary mr-2">Become Our Publisher</a>
                                    <a href="" class="btn btn-secondary">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <section class="block" id="patner">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h4 class="text text-center">3+ million developers use Postman to share, test, document & monitor APIs.</h4>
                        </div>
                    </div>
                    <div class="row list-logo">
                        <div class="col-md-2 col-sm-4 col-6">
                            <div class="item logo-microsoft" style="background-image: url('/images/logos.png');background-position: -1115px -20px;"></div>
                        </div>
                        <div class="col-md-2 col-sm-4 col-6">
                            <div class="item logo-box" style="background-image: url('/images/logos.png');background-position: 10px -122px;"></div>
                        </div>
                        <div class="col-md-2 col-sm-4 col-6">
                            <div class="item logo-real" style="background-image: url('/images/logos.png');background-position: -1116px -218px;"></div>
                        </div>
                        <div class="col-md-2 col-sm-4 col-6">
                            <div class="item logo-bbc" style="background-image: url('/images/logos.png');background-position: -754px -18px;"></div>
                        </div>
                        <div class="col-md-2 col-sm-4 col-6">
                            <div class="item logo-thoug" style="background-image: url('/images/logos.png');background-position: -390px -19px;"></div>
                        </div>
                        <div class="col-md-2 col-sm-4 col-6">
                            <div class="item logo-pivota" style="background-image: url('/images/logos.png');background-position: -4px -310px;"></div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="block bg-white">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="text mb-4">
                                <h1>Leading-edge technology</h1>
                                <p>A powerful GUI platform to make your API development faster & easier, from building API requests through testing, documentation and sharing.</p>
                                <p>We recommend the free Postman App for Mac, Windows, Linux or Chrome</p>
                                <a href="#" class="btn btn-primary">Content</a>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <figure>
                                <img src="images/express-api-development.png" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
            </section>
            <section class="block bg-purple">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <figure>
                                <img src="images/express-api-development.png" alt="">
                            </figure>
                        </div>
                        <div class="col-md-6">
                            <div class="text">
                                <h1>Quality support service</h1>
                                <p>A powerful GUI platform to make your API development faster & easier, from building API requests through testing, documentation and sharing.</p>
                                <p>We recommend the free Postman App for Mac, Windows, Linux or Chrome</p>
                                <a href="#" class="btn btn-secondary">Content</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="block" id="get-started">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="text">
                                <h1>Get Started</h1>
                                <p>Postman doesn’t require learning a new language, complicated UI, or new workflows. Developers can start using Postman immediately to make API development faster & easier.</p>
                                <a href="#" class="btn btn-primary">Become Our Publisher</a>
                            </div>
                        </div>
                        <div class="col-md-7"></div>
                    </div>
                </div>
            </section>
        </div>