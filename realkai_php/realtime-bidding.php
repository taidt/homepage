
        <div class="page-content">
            <header class="sub-page">
                <div class="mask">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h1>Maximise Your Websites Profits</h1>
                                <h2>Enjoy the real benefits of automatic advertising with us. We are happy to help you achieve your
                                    desired results.</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <section class="block bg-white">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h1 class="text text-center mb-5">What is realtime bidding</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 offset-md-1">
                            <!--IMG-->
                            <div class="img-bidding-demo">920 x 300</div>
                        </div>
                        <div class="col-md-8 offset-md-2">
                            <div class="mt-4">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad ex ipsa cum! Nostrum perferendis
                                    aut ut hic repellendus explicabo amet temporibus ducimus neque doloribus perspiciatis ea
                                    rerum provident, voluptatem porro.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="block bg-light-gray" id="product-unique">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h1 class="text text-center mb-5">How AdRoll Retargeting is Unique</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-reach.png">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="card-cus">
                                <div class="img-demo">100 x 100</div>
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-reach.png">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-reach.png">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-reach.png">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-reach.png">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="block" id="get-started">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="text">
                                <h1>Get Started</h1>
                                <p>Postman doesn’t require learning a new language, complicated UI, or new workflows. Developers
                                    can start using Postman immediately to make API development faster & easier.</p>
                                <a href="#" class="btn btn-primary">Become Our Publisher</a>
                            </div>
                        </div>
                        <div class="col-md-7"></div>
                    </div>
                </div>
            </section>
        </div>