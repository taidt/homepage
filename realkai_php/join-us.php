<div class="page-content">
            <header class="sub-page">
                <div class="mask">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h1>Maximise Your Websites Profits</h1>
                                <h2>Enjoy the real benefits of automatic advertising with us. We are happy to help you achieve your
                                    desired results.</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <section class="block bg-white">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h1 class="text text-center mb-5">How AdRoll Retargeting Works</h1>
                        </div>
                    </div>
                    <div class="row list-block">
                        <div class="col-md-4">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-step1.png" alt="Card image cap">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text"><i>Some quick example text to build on the card title and make up the bulk of the card's content.</i></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-step2.png" alt="Card image cap">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text"><i>Some quick example text to build on the card title and make up the bulk of the card's content.</i></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-step3.png" alt="Card image cap">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text"><i>Some quick example text to build on the card title and make up the bulk of the card's content.</i></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="block bg-light-gray join" id="example-ads">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h1 class="text text-center mb-5">The working positions</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="carousel">
                                <div class="indicators" id="indicators-carousel-ads">
                                    <a href="#ad728x90">
                                        <span>Seller</span>
                                    </a>
                                    <a href="#ad300x600">
                                        <span>Maketing</span>
                                    </a>
                                    <a href="#ad300x250">
                                        <span>Developer</span>
                                    </a>
                                </div>
                                <div class="owl-carousel inner" id="carousel-ads">
                                    <div class="item" data-hash="ad728x90">
                                        <div class="content text-left">
                                            <h1>Growth Marketer</h1>
                                            <h2>Mức lương lên tới $800</h2>
                                            <h3>Bạn sẽ làm gì ở BraveBits?</h3>
                                            <ul>
                                                <li>Nghiên cứu và phân tích thị trường/ khách hàng, hiểu được insight của họ và lập kế hoạch Marketing hiệu quả</li>
                                                <li>Thảo luận với các marketer và leader trong team để brainstorm các ý tưởng sáng tạo và thực hiện các chiến dịch thúc đẩy sản phẩm</li>
                                                <li>Viết copy cho sản phẩm và các chiến dịch marketing</li>
                                                <li>Phân tích website & KPI của sản phẩm để làm gia tăng tỉ lệ chuyển đổi ( Conversion rate) và trải nghiệm của người dùng (UX)</li>
                                            </ul>
                                            <h3>Bạn sẽ được gì ở BraveBits?</h3>
                                            <ul>
                                                <li>Nghiên cứu và phân tích thị trường/ khách hàng, hiểu được insight của họ và lập kế hoạch Marketing hiệu quả</li>
                                                <li>Thảo luận với các marketer và leader trong team để brainstorm các ý tưởng sáng tạo và thực hiện các chiến dịch thúc đẩy sản phẩm</li>
                                                <li>Viết copy cho sản phẩm và các chiến dịch marketing</li>
                                                <li>Thảo luận với các marketer và leader trong team để brainstorm các ý tưởng sáng tạo và thực hiện các chiến dịch thúc đẩy sản phẩm</li>
                                                <li>Viết copy cho sản phẩm và các chiến dịch marketing</li>
                                                <li>Phân tích website & KPI của sản phẩm để làm gia tăng tỉ lệ chuyển đổi ( Conversion rate) và trải nghiệm của người dùng (UX)</li>
                                                <li>Viết copy cho sản phẩm và các chiến dịch marketing</li>
                                                <li>Phân tích website & KPI của sản phẩm để làm gia tăng tỉ lệ chuyển đổi ( Conversion rate) và trải nghiệm của người dùng (UX)</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item" data-hash="ad300x600">
                                        <div class="content text-left">
                                            <h1>Growth Marketer</h1>
                                            <h2>Mức lương lên tới $800</h2>
                                            <h3>Bạn sẽ làm gì ở BraveBits?</h3>
                                            <ul>
                                                <li>Nghiên cứu và phân tích thị trường/ khách hàng, hiểu được insight của họ và lập kế hoạch Marketing hiệu quả</li>
                                                <li>Thảo luận với các marketer và leader trong team để brainstorm các ý tưởng sáng tạo và thực hiện các chiến dịch thúc đẩy sản phẩm</li>
                                                <li>Viết copy cho sản phẩm và các chiến dịch marketing</li>
                                                <li>Phân tích website & KPI của sản phẩm để làm gia tăng tỉ lệ chuyển đổi ( Conversion rate) và trải nghiệm của người dùng (UX)</li>
                                            </ul>
                                            <h3>Bạn sẽ được gì ở BraveBits?</h3>
                                            <ul>
                                                <li>Nghiên cứu và phân tích thị trường/ khách hàng, hiểu được insight của họ và lập kế hoạch Marketing hiệu quả</li>
                                                <li>Thảo luận với các marketer và leader trong team để brainstorm các ý tưởng sáng tạo và thực hiện các chiến dịch thúc đẩy sản phẩm</li>
                                                <li>Viết copy cho sản phẩm và các chiến dịch marketing</li>
                                                <li>Thảo luận với các marketer và leader trong team để brainstorm các ý tưởng sáng tạo và thực hiện các chiến dịch thúc đẩy sản phẩm</li>
                                                <li>Viết copy cho sản phẩm và các chiến dịch marketing</li>
                                                <li>Phân tích website & KPI của sản phẩm để làm gia tăng tỉ lệ chuyển đổi ( Conversion rate) và trải nghiệm của người dùng (UX)</li>
                                                <li>Viết copy cho sản phẩm và các chiến dịch marketing</li>
                                                <li>Phân tích website & KPI của sản phẩm để làm gia tăng tỉ lệ chuyển đổi ( Conversion rate) và trải nghiệm của người dùng (UX)</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item" data-hash="ad300x250">
                                        <div class="content text-left">
                                            <h1>Growth Marketer</h1>
                                            <h2>Mức lương lên tới $800</h2>
                                            <h3>Bạn sẽ làm gì ở BraveBits?</h3>
                                            <ul>
                                                <li>Nghiên cứu và phân tích thị trường/ khách hàng, hiểu được insight của họ và lập kế hoạch Marketing hiệu quả</li>
                                                <li>Thảo luận với các marketer và leader trong team để brainstorm các ý tưởng sáng tạo và thực hiện các chiến dịch thúc đẩy sản phẩm</li>
                                                <li>Viết copy cho sản phẩm và các chiến dịch marketing</li>
                                                <li>Phân tích website & KPI của sản phẩm để làm gia tăng tỉ lệ chuyển đổi ( Conversion rate) và trải nghiệm của người dùng (UX)</li>
                                            </ul>
                                            <h3>Bạn sẽ được gì ở BraveBits?</h3>
                                            <ul>
                                                <li>Nghiên cứu và phân tích thị trường/ khách hàng, hiểu được insight của họ và lập kế hoạch Marketing hiệu quả</li>
                                                <li>Thảo luận với các marketer và leader trong team để brainstorm các ý tưởng sáng tạo và thực hiện các chiến dịch thúc đẩy sản phẩm</li>
                                                <li>Viết copy cho sản phẩm và các chiến dịch marketing</li>
                                                <li>Thảo luận với các marketer và leader trong team để brainstorm các ý tưởng sáng tạo và thực hiện các chiến dịch thúc đẩy sản phẩm</li>
                                                <li>Viết copy cho sản phẩm và các chiến dịch marketing</li>
                                                <li>Phân tích website & KPI của sản phẩm để làm gia tăng tỉ lệ chuyển đổi ( Conversion rate) và trải nghiệm của người dùng (UX)</li>
                                                <li>Viết copy cho sản phẩm và các chiến dịch marketing</li>
                                                <li>Phân tích website & KPI của sản phẩm để làm gia tăng tỉ lệ chuyển đổi ( Conversion rate) và trải nghiệm của người dùng (UX)</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>