
        <div class="page-content">
            <header class="sub-page">
                <div class="mask">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h1>Maximise Your Websites Profits</h1>
                                <h2>Enjoy the real benefits of automatic advertising with us. We are happy to help you achieve your
                                    desired results.</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <section class="block bg-white">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h1 class="text text-center mb-5">How AdRoll Retargeting Works</h1>
                        </div>
                    </div>
                    <div class="row list-block">
                        <div class="col-md-4">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-step1.png" alt="Card image cap">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-step2.png" alt="Card image cap">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-cus">
                                <img class="card-img-top" src="https://www.adroll.com/assets/img/product/retargeting-product-step3.png" alt="Card image cap">
                                <div class="card-block">
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="block bg-light-gray" id="example-ads">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h1 class="text text-center mb-5">Advertiserment Size Example</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="carousel">
                                <div class="indicators" id="indicators-carousel-ads">
                                    <a href="#ad728x90">
                                        <span>Desktop</span><span>728x90px</span>
                                    </a>
                                    <a href="#ad300x600">
                                        <span>Desktop</span><span>300x600px</span>
                                    </a>
                                    <a href="#ad300x250">
                                        <span>Desktop & Mobile</span><span>300x250px</span>
                                    </a>
                                    <a href="#ad160x600">
                                        <span>Desktop</span><span>160x600px</span>
                                    </a>
                                    <a href="#ad320x50">
                                        <span>Mobile</span><span>320x50px</span>
                                    </a>
                                </div>
                                <div class="owl-carousel inner" id="carousel-ads">
                                    <div class="item" data-hash="ad728x90">
                                        <!--IMG-->
                                        <div class="ad-demo ad728x90">ad728x90</div>
                                    </div>
                                    <div class="item" data-hash="ad300x600">
                                        <!--IMG-->
                                        <div class="ad-demo ad300x600">ad300x600</div>
                                    </div>
                                    <div class="item" data-hash="ad300x250">
                                        <!--IMG-->
                                        <div class="ad-demo ad300x250">ad300x250</div>
                                    </div>
                                    <div class="item" data-hash="ad160x600">
                                        <!--IMG-->
                                        <div class="ad-demo ad160x600">ad160x600</div>
                                    </div>
                                    <div class="item" data-hash="ad320x50">
                                        <!--IMG-->
                                        <div class="ad-demo ad320x50">ad320x50</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="block" id="get-started">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="text">
                                <h1>Get Started</h1>
                                <p>Postman doesn’t require learning a new language, complicated UI, or new workflows. Developers
                                    can start using Postman immediately to make API development faster & easier.</p>
                                <a href="#" class="btn btn-primary">Become Our Publisher</a>
                            </div>
                        </div>
                        <div class="col-md-7"></div>
                    </div>
                </div>
            </section>
        </div>
        