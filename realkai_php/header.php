<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Realkai media</title>
    <link rel="stylesheet" href="dist/css/vendors.css">
    <link rel="stylesheet" href="./stylesheets/style.css">
</head>

<body>
    <div class="page-wrap">
        <!--NAVBAR-->
        <nav class="navbar-primary bg-white">
            <div class="navbar navbar-toggleable-md navbar-light">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <a class="navbar-brand" href="/">
                        <img src="https://cdn.realkai.com/logo.png" alt="">
                    </a>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active mr-2">
                                <a class="btn btn-secondary" href="#">Client Login</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-primary" href="#">Become our publisher</span></a>
                            </li>
                        </ul>
                    </div>
            </div>
        </nav>