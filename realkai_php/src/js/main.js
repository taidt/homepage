function fixHeightNavFixed() {
    var menuHeight = $('nav.navbar-primary').outerHeight();
    $('.page-content').css('padding-top', menuHeight + 'px');

    if ($('#term-fqa').length) {
        var nav_outer_w = $('#term-fqa .nav-fixed').outerWidth();
        var nav_outer_h = $('#term-fqa .nav-fixed').outerHeight();
        var before_nav_pos_top = $('#term-fqa .nav-fixed').offset().top;

        var parent_outer_h = $('#term-fqa').outerHeight();
        var parent_pos_top = $('#term-fqa').offset().top;
        var nav_padding_top = before_nav_pos_top - parent_pos_top;

        var footer_pos_top = $('footer').offset().top;

        var nav_stop_pos = 0;

        $(window).scroll(function () {
            $('#term-fqa').css('min-height', parent_outer_h);
            var window_scroll_top = $(this).scrollTop();
            var nav_scroll_pos_top = $('#term-fqa .nav-fixed').offset().top;
            var is_fixed_top = (window_scroll_top - (before_nav_pos_top - nav_padding_top)) >= 0;
            var is_fixed_bot = ((window_scroll_top + nav_outer_h + (nav_padding_top * 2)) - footer_pos_top) >= 0;
            if (is_fixed_top) {
                if (!is_fixed_bot) {
                    nav_stop_pos = nav_scroll_pos_top - before_nav_pos_top;
                    $('#term-fqa .nav-fixed').css({
                        'position': 'fixed',
                        'width': nav_outer_w,
                        'top': nav_padding_top
                    });
                } else {
                    $('#term-fqa .nav-fixed').css({
                        'position': 'absolute',
                        'width': nav_outer_w,
                        'top': nav_stop_pos
                    });
                }
            } else {
                $('#term-fqa .nav-fixed').css({
                    'position': 'static',
                    'width': nav_outer_w,
                    'top': 0
                });
            }
        });
    }
}

$(document).ready(function () {
    $('#carousel-ads').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        items: 1,
        center: true,
        autoHeight: true,
        dotsContainer: '#indicators-carousel-ads'
    });
    fixHeightNavFixed();
});